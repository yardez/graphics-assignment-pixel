﻿using UnityEngine;
using System.Collections;

public class FPSDisplay : MonoBehaviour
{
    float deltaTime = 0.0f;

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }


    void OnGUI()
    {
        {
            int w = Screen.width, h = Screen.height;

            GUIStyle style = new GUIStyle();

            Rect rect = new Rect(0, 0, w, h * 2 / 100);
            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 100;
            style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;
            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            GUI.Label(rect, text, style);
        }
        float yOffset = Screen.height / 40;

        if (yOffset < 15)
            yOffset = 15;

        displayText(Screen.width / 4, yOffset*0, "Controls");
        displayText(Screen.width / 4, yOffset*1, "Scene 1 - Main = 1");
        displayText(Screen.width / 4, yOffset*2, "Scene 2 - Boxes = 2");
        displayText(Screen.width / 4, yOffset*3, "Scene 3 - Physics = 3");
        displayText(Screen.width / 4, yOffset*4, "Scene 4 - Animation = 4");
        displayText(Screen.width / 4, yOffset*5, "Scene 5 - StressTest-1024 = 5");
        displayText(Screen.width / 4, yOffset*6, "Scene 6 - StressTest-1024-FB = 6");
        displayText(Screen.width / 4, yOffset*7, "Scene 7 - StressTest-4096 = 7");
        displayText(Screen.width / 4, yOffset*8, "Scene 8 - StressTest-4096-FB = 8");
        displayText(Screen.width / 4, yOffset*9, "Scene 9 - Superhot-Example = 9");

        displayText(Screen.width / 2, yOffset * 0, "Movement = WASD");
        displayText(Screen.width / 2, yOffset * 1, "Camera look = Mouse");
        displayText(Screen.width / 2, yOffset * 2, "Look At Target Effect Toggle = Left Click");
        displayText(Screen.width / 2, yOffset * 3, "Reset Scene = R");
        displayText(Screen.width / 2, yOffset * 4, "Toggle Camera Pixel Effect = P");
        displayText(Screen.width / 2, yOffset * 5, "Increase Camera Pixel Effect = +");
        displayText(Screen.width / 2, yOffset * 6, "Decrease Camera Pixel Effect = -");
        displayText(Screen.width / 2, yOffset * 7, "StressTest Effect Toggle = T");






    }

    //Simple gui label display function
    void displayText(float x, float y, string text)
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(x, y, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 3 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        GUI.Label(rect, text, style);

    }

}