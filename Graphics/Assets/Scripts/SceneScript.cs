﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneScript : MonoBehaviour
{
    Camera camera;
    GameObject pixelBox = null;
    public float pixelSize = 10.0f;

    // Use this for initialization
    void Start()
    {
        camera = GetComponent<Camera>();
        transform.gameObject.AddComponent<PixelEffectScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;

            //Mouse click effect toggle
            if (Physics.Raycast(ray, out hit))
            {
                print("I'm looking at " + hit.transform.name);
                if (hit.transform.GetComponent<PixelEffectScript>() != null)
                {
                    GameObject tempGameObject = hit.transform.GetComponent<PixelEffectScript>().getGameObject();
                    tempGameObject.SetActive(!tempGameObject.active);
                }
                else
                {
                    hit.transform.gameObject.AddComponent<PixelEffectScript>();
                }
            }
            else
                print("I'm looking at nothing!");
        }


        //Sets camera effect
        if (Input.GetKeyDown("p"))
        {
            PixelEffectScript newScript = transform.GetComponent<PixelEffectScript>();
            if (newScript != null)
            {
                Debug.Log("Script Found");
                pixelBox = newScript.getGameObject();
                newScript.setLocation(new Vector3(0, 0, 0));
                newScript.setPixelSize(pixelSize);
            }
            pixelBox.SetActive(!pixelBox.active);
        }

        if (Input.GetKeyDown("="))
        {
            if (pixelSize < 50)
                pixelSize++;
            PixelEffectScript newScript = transform.GetComponent<PixelEffectScript>();
            newScript.setPixelSize(pixelSize);
        }

        if (Input.GetKeyDown("-"))
        {
            if(pixelSize >2)
                pixelSize--;
            PixelEffectScript newScript = transform.GetComponent<PixelEffectScript>();
            newScript.setPixelSize(pixelSize);
        }


        if (Input.GetKeyDown("1"))
        {
            if(SceneManager.GetActiveScene().name != "Main")
            {
                SceneManager.LoadScene("Main");
            }
        }
        if (Input.GetKeyDown("2"))
        {
            if (SceneManager.GetActiveScene().name != "Boxes")
            {
                SceneManager.LoadScene("Boxes");
            }
        }
        if (Input.GetKeyDown("3"))
        {
            if (SceneManager.GetActiveScene().name != "Physics")
            {
                SceneManager.LoadScene("Physics");
            }
        }
        if (Input.GetKeyDown("4"))
        {
            if (SceneManager.GetActiveScene().name != "Animation")
            {
                SceneManager.LoadScene("Animation");
            }
        }
        if (Input.GetKeyDown("5"))
        {
            if (SceneManager.GetActiveScene().name != "StressTest-1024")
            {
                SceneManager.LoadScene("StressTest-1024");
            }
        }
        if (Input.GetKeyDown("6"))
        {
            if (SceneManager.GetActiveScene().name != "StressTest-1024-FastBox")
            {
                SceneManager.LoadScene("StressTest-1024-FastBox");
            }
        }
        if (Input.GetKeyDown("7"))
        {
            if (SceneManager.GetActiveScene().name != "StressTest-4096")
            {
                SceneManager.LoadScene("StressTest-4096");
            }
        }
        if (Input.GetKeyDown("8"))
        {
            if (SceneManager.GetActiveScene().name != "StressTest-4096-FastBox")
            {
                SceneManager.LoadScene("StressTest-4096-FastBox");
            }
        }

        if (Input.GetKeyDown("9"))
        {
            if (SceneManager.GetActiveScene().name != "Superhot-Example")
            {
                SceneManager.LoadScene("Superhot-Example");
            }
        }

        if (Input.GetKeyDown("r"))
        {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKey("escape"))
            Application.Quit();


    }
}