﻿using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
//using UnityEditor;

public class PixelEffectScript : MonoBehaviour {

    public Camera playerCamera;
    public float pixelSize = 10.0f; //Pixel size of effect
    public bool createChild = true; //Used to automatically set up effect from character, can be disabled to allow artist/programmer control.
    public bool fastBox = true; //Render effect around a box instead of pixel perfect(ish) mesh
    public float effectSize = 1.0f; //Used to scale effect mesh around exsisting mesh
    public bool disableEffect = false;
    public bool UpdateEffect = false; //used if object grows/shrinks in size. Unity mostly handles this with parent/child relationship so is often not needed.
    public bool groupedModels = false; //Used to toggle child mesh catching
    GameObject newGameObject = null;

    public bool attachToChildren = false;

    public float UpdateEffectDelay = 0.0f; //Specify delay taken after effect boundry doesn't need updating, performance.
    bool doUpdateEffect = true;
    float timeTillUpdateEffectDelay = 0.0f;


    bool hasRenderer = false;

    public GameObject getGameObject()
    {
        return newGameObject;
    }

    public void setLocation(Vector3 newVec)
    {
        newGameObject.transform.localPosition = newVec;
    }

    public void setPixelSize(float newPixelSize)
    {
        pixelSize = newPixelSize;
        newGameObject.GetComponent<Renderer>().material.SetFloat("_PixelSize", pixelSize);
    }


    // Use this for initialization
    void Start() {



        Renderer tempRenderer = GetComponent<Renderer>();


        if (attachToChildren)
        {

            foreach (Transform child in GetComponentsInChildren<Transform>()) //checks all children of an object
            {
                if (child.GetComponent<PixelEffectScript>() == null) //If child already has CellAdjust script, ignore
                {
                    GameObject tempGameObject = child.gameObject;
                    tempGameObject.AddComponent<PixelEffectScript>();
                    PixelEffectScript tempScript = tempGameObject.GetComponent<PixelEffectScript>();

                    tempScript.pixelSize = pixelSize;
                    tempScript.createChild = createChild;
                    tempScript.fastBox = fastBox;
                    tempScript.effectSize = effectSize;
                    tempScript.disableEffect = disableEffect;
                    tempScript.UpdateEffect = UpdateEffect;
                    tempScript.groupedModels = groupedModels;
                    tempScript.attachToChildren = attachToChildren; //Incase of renderer children underneath
                }
            }

            


            if (tempRenderer == null)
                return; //Return if GameObject doesn't have a renderer
            else
                hasRenderer = true;
        }

        //Create child and attach scripts to that child
        if (createChild)
        {
           

            //Uses a simple cube for effect, best for objects with holes in it, such as sails on a windmill or a chain fence
            if (fastBox)
            {
                newGameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
                Destroy(newGameObject.GetComponent(typeof(BoxCollider)));
            }
            else //Use the original mesh for effect
            {
                Mesh mesh = GetComponent<MeshFilter>().mesh;
                Bounds bounds = mesh.bounds;
                newGameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
                newGameObject.GetComponent<MeshFilter>().mesh = mesh;
                Destroy(newGameObject.GetComponent(typeof(BoxCollider)));
            }

            newGameObject.name = name + "-EffectMesh"; //Sets name for clarity in editor scene view

            if (groupedModels)
            {
                if (fastBox)
                {
                    
                    Bounds newBounds = CalculateLocalBounds();
                    newGameObject.transform.position = newBounds.center;
                    newGameObject.transform.localScale = newBounds.size * effectSize;
                    newGameObject.transform.parent = transform;
                }
                else
                {
                    newGameObject.transform.parent = transform;
                    newGameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f) * effectSize;
                    newGameObject.transform.position = transform.position;
                    newGameObject.transform.rotation = transform.rotation;


                }
            }
            else
            {
                //

                if(tempRenderer != null)
                {
                    newGameObject.transform.position = GetComponent<Renderer>().bounds.center;
                    newGameObject.transform.localScale = GetComponent<Renderer>().bounds.size * effectSize;
                }
                newGameObject.transform.parent = transform;
            }
        }
        else //If child create is not selected. used when artist/programmer wants fine control over the effect. - Same as applying the shader manually
        {
            newGameObject = gameObject;
        }

        // Assign effect material to the object
        if (!disableEffect)
        {
            Material newMat = Resources.Load("Shaders/CensorShader", typeof(Material)) as Material;
            newGameObject.GetComponent<Renderer>().material = newMat;
            newGameObject.GetComponent<Renderer>().material.SetFloat("_PixelSize", pixelSize);
        }

    }
	
	// Update is called once per frame
	void Update ()
    {

        if (attachToChildren && hasRenderer == false)
        {


            return;
        }

        if (fastBox)
        {
            
            if (UpdateEffect && doUpdateEffect)//Used when effect needs adjusting as the scene develops.
            {

                Bounds newBounds = CalculateLocalBounds();
                newGameObject.transform.position = newBounds.center;

                if(!(newGameObject.transform.localScale == newBounds.size * effectSize))
                {
                    newGameObject.transform.localScale = newBounds.size * effectSize;
                }
                else
                {
                    doUpdateEffect = !doUpdateEffect;
                }
            }
            else
            {
                if((timeTillUpdateEffectDelay <= UpdateEffectDelay))
                {
                    timeTillUpdateEffectDelay += Time.deltaTime;
                }
                else
                {
                    timeTillUpdateEffectDelay = 0.0f;
                    doUpdateEffect = !doUpdateEffect;
                }
            }

        }

    }

    private Bounds CalculateLocalBounds()
    {
        Quaternion currentRotation = this.transform.rotation;
        this.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        Bounds bounds = new Bounds(this.transform.position, Vector3.zero);
        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            if (!(renderer.transform.name == newGameObject.name))//Don't readd the effect bounding box
            {
                bounds.Encapsulate(renderer.bounds);
            }
            else
                Debug.Log("Chips");

        }
        Debug.Log("The local bounds of this model is " + bounds);
        return bounds;
    }

}