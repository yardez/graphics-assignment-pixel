﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

public class StressScript : MonoBehaviour {


    public int rowSize = 10;
    public int colSize = 10;
    public float gapSize = 2f;
    bool hasChecked = false;
    public bool isFastBox = false;
    List<GameObject> listOfBalls = new List<GameObject>();
    // Use this for initialization
    void Start() {


        for (int i = 0; i < colSize; i++)
        {
            for (int j = 0; j < rowSize; j++)
            {
                if (!isFastBox)
                {
                    GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/StressBall"));
                    go.transform.position = new Vector3(((j * gapSize)), 2, ((i * gapSize)));
                    go.transform.parent = transform;
                }
                else
                {
                    GameObject go = (GameObject)Instantiate(Resources.Load("Prefabs/StressBall-FastBox"));
                    go.transform.position = new Vector3(((j * gapSize)), 2, ((i * gapSize)));
                    go.transform.parent = transform;
                }
            }
        }


    }

    void Update()
    {
        if (Input.GetKeyDown("t"))
        {
            checkBalls();
            for (int i = 0; i < listOfBalls.Count; i++)
            {
                listOfBalls[i].SetActive(!listOfBalls[i].active);
            }
        }

        // Update is called once per frame

    }

    void checkBalls()
    {
        if(!hasChecked)
        {
            GameObject tempObject = GameObject.Find("StressGroup");
            foreach (PixelEffectScript script in tempObject.GetComponentsInChildren<PixelEffectScript>())
            {
                listOfBalls.Add(script.getGameObject());
            }
            hasChecked = true;
        }

    }

}
