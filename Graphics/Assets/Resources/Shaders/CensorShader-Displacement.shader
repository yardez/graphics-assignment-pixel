﻿Shader "Custom/CensorShader-Displacement"
{
	Properties
	{
		_PixelSizeEditor("Pixel Size - Editor", Float) = 10
		_PixelSize("Pixel Size - Script Adjusted", Float) = 10
	}

	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" }
		Blend Off
		Lighting Off
		Fog{ Mode Off }
		ZWrite Off
		LOD 200
		Cull Off //Allows the effect to show incase of inverted normals

		GrabPass{ "_GrabTexture" } // Grabs section of screeen behind the shader's object

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f //Structure used to story position and UV data
			{
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
			};

			float _PixelSize; 

			v2f vert(appdata_base v) //Work out vertex infomation and screen location for the fragment shader.
			{
				v2f output;
				output.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				output.uv = ComputeGrabScreenPos(output.pos); 
				return output;
			}

			sampler2D _GrabTexture;

			//Use the newly modifed data to apply the pixelated effect to the texture.
			float4 frag(v2f Input) : COLOR
			{
				float2 steppedUV = Input.uv.xy / Input.uv.w;

				steppedUV /= _PixelSize / _ScreenParams.xy;
				steppedUV = round(steppedUV);
				steppedUV *= _PixelSize / _ScreenParams.xy;
				return tex2D(_GrabTexture, steppedUV);
			}
			
			ENDCG
		}
	}
}